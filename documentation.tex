\documentclass[12pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}    % Extended typesetting of mathematical expression.
\usepackage{amssymb}    % Provides a multitude of mathematical symbols.
\usepackage{mathtools}  % Further extensions of mathematical typesetting.
\usepackage{footnote}
\usepackage{url}
\usepackage{listings}
\usepackage{enumitem}
\setlist[enumerate]{itemsep=0mm, topsep=0mm}
\setlist[itemize]{itemsep=0mm, topsep=0mm}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\DeclareMathOperator*{\minimize}{minimize}
\DeclareUnicodeCharacter{00A0}{ }
\makesavenoteenv{tabular}
\makesavenoteenv{table}

\lstset{ %
  breakatwhitespace=true,         % sets if automatic breaks should only happen at whitespace
  breaklines=true  ,               % sets automatic line breaking
  frame=single,
  basicstyle=\footnotesize, 
  language=Prolog,    
  numbers=left,
  showstringspaces=false
}

\title{Vehicle Routing Problem solving with Answer Set Programming}
\author{André Schidler}
\begin{document}
\maketitle
\begin{abstract}
The vehicle routing problem is a long known intractable problem. It asks the same questions logistic companies ask every day: how to plan delivery routes to satisfy all customers?\\
This paper adds an exact declarative approach using answer set programming to the extensive research performed on the problem.
\end{abstract}
\section{Introduction}
Scheduling is a problem that is prevalent in today's life. From scheduling appointments to managing logistic companies, while the tasks may become more intricate, the challenge stays the same: using resources efficiently and without conflict.\\
Although it might be desirable to automate such tasks, it often is surprisingly hard, as it is quite common that scheduling tasks are instances of computationally intractable problems.

One such problem is the \emph{vehicle routing problem (VRP)}\footnote{To be exact, the VRP is a combinatorial problem, but the time window extension puts it in the class of scheduling problems}, an optimization problem closely tied to logistics, where routes for delivery vehicles are planned. This affiliation has made it the subject of extensive research and numerous approaches have been developed in the almost sixty years since its definition.

The project presented in this document tried to gauge the applicability of \emph{answer set programming (ASP)} to the vehicle routing problem. \\
This document will first introduce some VRP and ASP preliminaries, before delving into the full formal definition of the VRP as used in this project. Afterwards the created solution is presented and the document concludes with results found during the project.


\subsection{Answer Set Programming}
ASP is a declarative problem modelling and solving framework. Problems and instances are encoded as logic programs, usually as a set of implications on atoms and their negations, called rules \cite{asp}.

ASP uses default negation, therefore negated atoms evaluate to true, if either the atom is false or not known to be true. This allows for defining defaults, hence the name. As an example, the following code states that if the sun is shining, and we don't know that it rained (so it either did not rain, or we don't know if it rained), we assume that it is dry.
\begin{lstlisting}
dry :- sun, not rained.
\end{lstlisting}

The result of an ASP program is a set answer sets or stable models. A model is a set of atoms that are assumed to be true, without leading to a contradiction. In order to validate if a model $M$ is a stable model, the reduct of the program is calculated as follows:
\begin{enumerate}
\item All rules where $M$ contradicts the negations are removed. 
\item The negations are removed from the remaining rules.
\end{enumerate}

The resulting reduct is therefore negation free and it is possible to calculate the minimal model for the reduct\footnote{
Please note that in case of disjunctions more than one such model may exist. Since the programs used in this project are disjunction free, this case is not examined further. More information can be found in the materials of the Potassco project \cite{potassco}.}
(i.e. a model where not real subset is a model). 
If $M$ is equal to the resulting model, it is stable.

Rules may use variables instead of constants, where the domain of a variable is determined by the atoms it appears in.\\
If a program contains variables, an extra step, called the grounding step, is required to attain a variable free program. During grounding variables are replaced by constants. This is done by duplicating the rule and replacing the variables by every possible combination of values. Therefore a rule containing three variables, each having a domain of size 5, results in 125 rules.\\
Variables are a a good way to keep the encoding concise and create problem encodings that are applicable to different instances, but they may also heavily impact performance.

\subsection{Vehicle Routing Problem}
Companies dealing with logistics have to decide which delivery vehicle services which customer on a regular basis. This problem is the basis for the VRP.
Its instances define a set of customers (nodes) with paths between them (often a graph) and a solution is a route (sequence of nodes) for each vehicle, where all customers get their deliveries. Vehicles start and end their route at a depot (sometimes more than one) and it is common to define either a time or a capacity limit for the vehicles \cite{survey}.\\
The VRP is an optimization problem (the routes are optimal according to some cost metric) and it is a generalization of the travelling salesmen problem \cite{original}, the associated decision problem is therefore NP-hard.

Many extensions to the VRP exist. Including time windows (a node must be visited during specific times), site dependency (not every vehicle may service every node) or allowing multiple depots \cite{survey,vrptw,complete}.\\



\section{Background}
The formal definition for the VRP varies from source to source, as it is often adapted to the solving method. While the core idea stays the same, some details are often different \cite{survey,vrptw,complete}. Sometimes the customers are nodes in a graph, which in turn is sometimes required to be complete. Other definitions use points on a 2-D plane instead of graphs. Nodes must be visited exactly once according to some definitions or can be visited multiple times according to others. Not every source distinguishes between costs and durations.

The input instances used in this project use a directed graph to define the instance.\\
It is allowed for each node to be visited more than once (either by the same or a different vehicle). This allows for leaf nodes (the predecessor needs to be visited at least twice) and ``choke nodes'', where multiple vehicles must use the same node.\\
The instances have to define a cost and a duration for each edge. The duration is used in connection with a time limit and the costs are used for optimization. Having both costs and duration allows for greater flexibility.

Formally a VRP instance used for this project has to define the following elements:
\begin{enumerate}
	\item A graph $G = (V,E)$ with nodes $V$ and edges $E$.
	\item A mapping from edges to integers, called duration: $d: E \to \mathbb{N}$.
	\item A mapping from edges to integers, called cost: $c: E \to \mathbb{N}$.
	\item A mapping from nodes to integers, called the service time: $d: C \to\mathbb{N}$.
	\item A time limit $l \in \mathbb{N}$.
	\item A limit on the number of vehicles $n \in \mathbb{N}$.
\end{enumerate}

As discussed above the solution to a VRP instance are routes, one for each vehicle. Since this definition allows for a node to be visited by more than one vehicle, a solution also defines which vehicle provides the service to the customer.\\
The routes use the concept of steps, in one step the vehicle moves from one node to another, where the first step is the move out of the depot to the first node and the last one the return to the depot. The maximum number of steps is assumed to be limited by $l$.\\
The formal definitions are as follows:
\begin{itemize}
	\item A route is defined for every vehicle $v \leq n$ as follows: \\
		$r_v(x,i) =
		\begin{cases}
			1	\quad \text{if vehicle } v \text{ is at node } x \in V \text{ at step } i \in \mathbb{N}\\
			0	\quad \text{else}
		\end{cases}
		$
	\item The service mapping is defined for each node $x \in V$ as follows: \\
		$s_x(v) =
		\begin{cases}
			1	\quad \text{if vehicle } v \text{ services node } x\\
			0	\quad \text{else}
		\end{cases}
		$
\end{itemize}
As a shorthand for the number of steps in a route the notation $|r_v|$ is used.

Each solution must adhere to constraints. Each route must start and end at the depot (known as the 0 node) and must not exceed the time limit. Formally the following constraints must hold for every solution:
\begin{enumerate}
	\item Routes must start at the depot:
		$$r_v(0,1) = 1 \quad \text{for all } v \in [1..n]$$
	\item Routes must end at the depot:
		$$r_v(0,|r_v|) = 1 \quad \text{for all } v \in [1..n]$$
	\item A vehicle is at exactly one node at each step: 
		$$\sum_{x \in V} r_v(x,i) = 1 \quad \text{for all } v \in [1..n], i \in [1..|r_v|]$$
	\item Each node is serviced by exactly one vehicle:
		$$\sum_{v=1}^n s_x(v) = 1 \quad \text{for all } x \in V  \setminus \{0\}$$
	\item Each node transition must exist as an edge: 
		$$\sum_{x,y \in V, (x,y) \not\in E} \sum_{v=1}^n \sum_{i=2}^{|r_v|} (r_v(x,i-1) * r_v(y,i)) = 0 $$
	\item A vehicle that services a node must visit it: 
		$$\sum^{|r_v|}_{i=1} r_v(x,i) \geq s_x(v) \quad \text{for all } v \in [1..n], x \in V $$
	\item The total travel duration is within the limit. The travel duration is calculated by adding the summed duration of all edges in the route to the combined service time of all serviced nodes.
		\begin{equation*}\begin{split}
		\sum_{i=2}^{|r_v|} \sum_{(x,y) \in E} (d(x,y) * r_v(x,i-1) * r_v(y,i)) + \\
		\sum_{x \in V}(d(x) * s_x(v)) \leq l \quad \text{for all } v \in [1..n]
		\end{split}\end{equation*}
	\item The depot is not serviced: 
		$$\sum_{v = 1}^ns_0(v) = 0$$

\end{enumerate}

Finally, the VRP is an optimization problem. Each edge has an associated cost and the goal is to minimize the total cost over all routes:
$$\displaystyle{\minimize \sum_{v=1}^n \sum_{i=2}^{|r_v|}\sum_{(x,y) \in E} (c(x,y) * r_v(x,i-1) * r_v(y,i))}$$

\subsection*{Time Window Extension}
\emph{The time window extension to the VRP (VRPTW)} adds another constraint to the original problem. Each customer only accepts deliveries within defined time windows.

Formally, an instance additionally defines a set $W_x \subset \mathbb{N}$ of times for each node $x \in V \setminus \{0\}$ ($W_x$ is assumed to be finite).

As each VRPTW instance is also a VRP instance, if we ignore the time windows, we assume $S$ is a solution for the VRP part of an instance.\\
In order to define the constraints that must hold for $S$ to be a valid solution for the VRPTW instance, we define two mappings:
\begin{enumerate}
	\item For each vehicle $v \in [1..n]$ we define the arrival time $a_v: \mathbb{N} \to \mathbb{N}$. The arrival time is a mapping from a step number to the time elapsed.
	\item For each node $x \in V \setminus \{0\}$ we define the service step $t_x(i)$ as:
		$$t_x(i) = \begin{cases}
			1	\quad \text{if node } x \text{ is serviced at step } i \in \mathbb{N}\\
			0	\quad \text{else}
		\end{cases}$$ 
\end{enumerate}

If given $S$ we can find such mappings, that all of the following conditions hold, $S$ is a valid VRPTW solution for the instance:
\begin{enumerate}
	\item A node is only serviced once:\\
		$$\sum^l_{i=1} t_x(i) = 1 \quad \text{for all } x \in V \setminus \{0\}$$
	\item The servicing vehicle must be at the node at the service step:\\
		$$\sum_{v = 1}^n \sum_{i=1}^{|r_v|} (s_x(v) *  t_x(i) * r_v(x,i)) = 1 \quad \text{for each } x \in V \setminus \{0\}$$
	\item Arrival times must be consistent with the previous arrival. This means that we cannot arrive at a time earlier than the arrival time at the previous node, plus edge duration and service time at the previous node:\\
		\begin{equation*}\begin{split}
			a_v(i) \geq a_v(i-1) + 
			\sum_{(x,y) \in E} (d(x,y) * r_v(x,i-1) * r_v(y,i)) +\\ 
			\sum_{x \in V} (d(x) * r_v(x, i-1) * s_x(v) * t_x(i))
			\\ \text{for all } v \in [1..n], i \in [1..|r_v|]
		\end{split}\end{equation*}
	\item The total duration must not exceed the allowed duration: \\
		$$a_v(|r_v|) \leq l \quad \text{for all } v \in [1..n]$$
	\item The arrival times must be consistent with the time windows:\\
		$$a_v(i) \in W_x \quad \text{if } r_v(x,i)  = 1 \text{, for all } v \in [1..n], i \in [1..|r_v|]$$
\end{enumerate}

\section{Solution}
In this section we describe the two ASP encodings created in the course of the project. The first one encodes the VRP problem and the second encoding extends it to the VRPTW problem.\\
Furthermore a script that visualizes the results created by clingo \cite{potassco} and an instance generator are presented here\footnote{All sources can be found at \url{https://bitbucket.org/aschidler/vrp.git}}.

\subsection{VRP Encoding}
The encoding presented in this section was designed to encode the VRP problem and can be found in the file ``vrp.lp''. It is separated into four parts, which will subsequently be discussed:
\begin{enumerate}
	\item Auxiliary atoms
	\item Route generation
	\item Constraint checking
	\item Symmetry breaking
\end{enumerate}

In the following code listings, the explanation of the code follows the listing. Furthermore some listings contain comments that are preceded by ``\%''.

\subsubsection{Auxiliary variables}
This part of the encoding introduces some atoms that are lower arity versions of input variables.\\
Additionally some limits are defined here.\\

\begin{lstlisting}
% introduce edge variables if necessary
edge(X,Y) :- cost(X,Y,_).
% introduce node variables
node(X) :- cost(X,_,_).
node(Y) :- cost(_,Y,_).

% Define limits for steps and number of cycles. Finding a tight heuristic minimizes grounding
num(1..L) :- timeLimit(T), T * 3/ 2 = L.
vehicleNum(1..M) :- #count{V:vehicle(V)} = C, 3 * C = M.
\end{lstlisting}

The first five lines define atoms for nodes and edges derived from the cost information of the input instance. Their usage makes the code more intuitive and may reduce grounding time.\\
The last two lines define limits for step size per cycle (num) and the number of total cycles (vehicleNum). These depend on the input instance and are required to limit the domain size of integers used later in the encoding. Both values are based on heuristics and therefore are usually not tight. They may also cause solving to fail in some rare edge cases, where we have few vehicles and a lot of very small cycles.

\subsubsection{Route generation}
The purpose of this part is to produce solution candidates, which are then checked against the constraints in the next part.\\

\begin{lstlisting}
leave(1).
routeOrder(0,Y,1,V) :- leave(V), edge(0,Y), not dontOrder(0,Y,1,V).
% If not at the depot, simply choose a successor
routeOrder(X,Y,N,V) :- routeOrder(_,X,N-1,V), X != 0, edge(X,Y), num(N), not dontOrder(X,Y,N,V).
% Avoid that the same ordinal is used twice
dontOrder(X,Y',N,V) :- routeOrder(X,Y,N,V), edge(X,Y'), Y != Y'.
\end{lstlisting}

The idea here is to start a cycle and try to find a way back to the depot. The first line simply states, that we have at least one cycle. The ''leave(k)'' ($k \in \mathbb{N}$) atoms define at which cycle we are currently at.\\
Line 2 and 4 construct the actual route. In case we are at the depot (line 2), we need a leave to start the cycle. At any other node a random edge is selected, until we reach the depot or run out of steps.\\
The last line ensures that only one edge is selected per step.\\

\begin{lstlisting}
visited(X,V) :- routeOrder(_,X,_,V).
visited(X,V) :- visited(X,V-1), leave(V).
% If route ends at the depot, start new cycle, if there are unvisited nodes
leave(V) :- node(X), vehicleNum(V), visited(0,V-1), V > 1, not visited(X,V-1).
\end{lstlisting}

In order to limit the search space, the number of cycles has to be limited. Here it is ensured, that no new cycle is started once all nodes have been visited. In lines 2 and 3 a list of visited nodes for all previous cycles is managed. The last one starts a new cycle, whenever after a completed cycle unvisited nodes remain.\\

\begin{lstlisting}
% Each route must terminate
:- leave(V), not visited(0,V).
\end{lstlisting}

As there is a limit for steps in a cycle, it may happen, that a cycle does not reach the depot. In this case we can immediately stop.\\

\begin{lstlisting}
assign(C,V) :- leave(C), vehicle(V), not notAssign(C,V).
notAssign(C,V') :- assign(C,V), vehicle(V'), V != V'.
\end{lstlisting}

The number of cycles may exceed the number of vehicles. Therefore the cycles need to be assigned to vehicles. The first line assigns the cycles randomly and the second ensures that each cycle is assigned only once.

In the first attempt, route generation randomly selected one or more outgoing edges per node. As this yields a lot of non-connected routes, which may not start or end at the depot, this proved to be inefficient.\\
Another attempt to encode most of the problem with a constraint satisfaction problem extension (clingcon \cite{potassco}), yielded worse results. This may be due to the approach not being suitable to the VRP.\\
The approach presented above yielded the best results.

\subsubsection{Constraint checking}
The route generation already ensures that most of the constraints hold. The only constraint that is left, is the maximum travel duration for each vehicle.\\

\begin{lstlisting}
% Make sure every node is serviced, instroduces symmetries
servicesC(X,C) :- routeOrder(_,X,_,C), X != 0, not notServices(X,C).
notServices(X,C) :- servicesC(X,C'), leave(C), C' != C.
:- node(X), not servicesC(X,_), X != 0.

% This is needed to avoid counting the service time multiple times
servicesAt(X,N,C) :- servicesC(X,C), routeOrder(_,X,N,C), not notServicesAt(X,N,C).
notServicesAt(X,N,C) :- servicesAt(X,N',C), routeOrder(_,X,N,C), N != N'.
\end{lstlisting}

As already mentioned, a vehicle can visit a node multiple times and a node can be visited by multiple vehicles.\\
It is therefore necessary to determine which vehicle services which node, as servicing a node takes extra time.\\
The rules in the listing above select which node is serviced in which cycle and thereby implicitly by which vehicle.
The first two statements assign each node to exactly one cycle. Line 4 ensures that every node (except the depot) is serviced.\\
The last two lines determine at which step exactly the service will take place. This is required as a vehicle may visit the same node multiple times.\\

\begin{lstlisting}
% Calculate the travel time for each cycle
travelTime(0,0,C) :- leave(C).
travelTime(N,D,C) :- travelTime(N-1,D',C), num(N), routeOrder(X,Y,N,C), duration(X,Y,T), timeLimit(L), L >= D, D = D' + T, not servicesAt(Y,N,C).
travelTime(N,D,C) :- travelTime(N-1,D',C), num(N), routeOrder(X,Y,N,C), duration(X,Y,T), timeLimit(L), L >= D, D = D' + T + ST, servicesAt(Y,N,C), serviceTime(Y,ST).
travelTime(D,C) :- routeOrder(_,0,N,C), travelTime(N,D,C).
\end{lstlisting}

These lines calculate the actual travel time for each cycle. The times are added up step by step.\\
The first statement starts the calculation for each cycle. In lines 3 and 4 the time is added for each step. There is a case distinction: if the vehicle services the node at this step, the service time is added as well as the edge duration, otherwise only the edge duration is used.
The last line defines the sum at the last step as the total for the cycle.\\

\begin{lstlisting}
% This occurs whenever a cycle exceeds the time limit
:- leave(C), not travelTime(_,C).
\end{lstlisting}

Whenever the travel time of a cycle exceeds the time limit, the calculation stops. This statement checks for absence of the calculation and therefore ensures that no cycles that are too long exist.\\

\begin{lstlisting}
% Aggregate times for vehicles
vehicleTime(0,0,V) :- vehicle(V).
vehicleTime(T,C,V) :- vehicleTime(T',C-1,V), vehicleNum(C), assign(C,V), travelTime(D,C), T = T' + D, timeLimit(L), T <= L.
vehicleTime(T,C,V) :- vehicleTime(T,C-1,V), vehicleNum(C), not assign(C,V).
% Time limit exceeded
:- leave(C), vehicle(V), not vehicleTime(_,C,V).
\end{lstlisting}

In order to check if a vehicle does not exceed the time limit, a sum over the travel time of all assigned cycles is required. Line 2 initializes the counter for each vehicle with 0. Now each cycle is processed in increasing order. If a cycle is not assigned to the current vehicle, the duration is not added (line 4). If it is assigned to the vehicle, add the duration, if the time limit is not exceeded.\\
The last statement checks, if the calculation succeeded for each vehicle.\\

\begin{lstlisting}
% Optimize for costs
#minimize{C,N,V : routeOrder(X,Y,N,V), cost(X,Y,C)}.
\end{lstlisting}

This statement defines that the solver should look for the best solution in terms of summed up costs.

\subsubsection{Symmetry breaking}
A common problem whenever a problem uses some kind of ordering are so called symmetries. Symmetries are essentially different solutions that are equal for the problem that is being solved. They increase the searchspace and therefore hinder performance.\\
As an example consider vehicles in the context of the VRP. They are interchangeable: given a solution and two vehicles $v_1$ and $v_2$, we could replace any occurrence of $v_1$ by $v_2$ and vice versa, the result would be a different solution that is equal to the original one.\\
Formally a symmetry is a permutation on the atoms of a program that yields the same program. To continue our example, assume that $v_1,...,v_n$ are vehicles, the following are some permutations that yield the same program: $\pi_1 = (v_1\: v_n), \pi_2 = (v2\: v_n), \pi_3 = (v_1\: v_2)\: (v_2\: v_n)$\footnote{This is a little simplified. Since vehicles are not encoded as atoms, each atom containing one vehicle must be replaced by the same atom containing the other one, resulting in potentially very large permutations.} \cite{symmetry}.\\
This in turn means that the permutation applied on a solution results in a different but equal solution. This property hugely increases the search space for the solver, imagine how many permutations with ten vehicles exist (about 3,6 million).\\
The symmetry breaking part of the encoding tries to remove equivalent solutions. The following rules are designed to remove one part of the symmetry as a viable solution.\\

\begin{lstlisting}
% The first vehicle goes first.
drives(V) :- assign(C,V).
:- drives(V), vehicle(V'), V > V', not drives(V').
\end{lstlisting}

The assignment of cycles to vehicles is not important, as only the partitioning matters, but not which vehicle is assigned which partition. This code ensures, that the cycles are assigned in ascending order.\\

\begin{lstlisting}
% Order entering the first node by vehicle ordering
:- routeOrder(0,X,1,V), routeOrder(0,X',1,V'), V < V', X > X'.
\end{lstlisting}

The numbering of the cycle is also irrelevant, as it does not matter which cycle is the first. The code tries to order the cycles by their first node, which may fail if this node is shared by several vehicles.\\

\begin{lstlisting}
% No edge should be used twice, that makes no sense in the VRP scenario (but may be useful in the time window extension)
:- routeOrder(X,Y,N,V), routeOrder(X,Y,N',V), N != N'.
\end{lstlisting}
Using the same edge twice is only part of an optimal solution in rare edge cases, but has a large impact on performance. Allowing this renders the encoding essentially unusable. Therefore this rule excludes such routes.\\

\begin{lstlisting}
% Set a goal for each new cycle. Use minimum to make it deterministic.
unvisited(X,V-1) :- leave(V), node(X), X != 0, not visited(X,V-1).
goal(X,V) :-  #min{X': unvisited(X',V-1)} = X, leave(V).
% The leave check is just so the aggregate above is guaranteed to be calculated at this point
:- goal(X,V-1), not visited(X,V-1), leave(V).
\end{lstlisting}

Disregarding optimality for a second, creating the same cycle twice, or a sub-cycle is still correct (if the limits still hold). In line 2 a list of unvisited nodes is managed, which is used in the next line to choose a goal for the next cycle. Setting a goal ensures that the new cycle differs from the previous cycles. The minimum is used to avoid symmetries from selecting a random node. The last line stops whenever the goal is not met.\\

\begin{lstlisting}
% Kill Symmetry, i.e. always choose the first time entering as servicing time
:- servicesAt(X,N,C), notServicesAt(X,N',C), N' < N.
\end{lstlisting}
For the VRP it only matters which vehicle services a node, not at which step. Therefore we can always choose the soonest possible step.

This concludes the VRP encoding, in the next section we will examine the VRPTW extension.

\subsection{VRPTW Encoding}
The time window encoding is a direct extension of the VRP encoding. There are two tasks the extension performs:
\begin{enumerate}
	\item Check if all the nodes can be serviced within their time windows.
	\item Check if the total travel duration still holds.
\end{enumerate}

Both checks require a timeline for each vehicle, which in turn requires an ordering of the assigned cycles.\\
Please note that vehicles may leave the depot at different times, and they may also wait at a node in order to meet a time window.\\

\begin{lstlisting}
%First decide at what time a node is serviced. This is necessary, in case a node is visited multiple times
{servicedAtTw(N,C) : routeOrder(_,Y,N,C), servicesC(Y,C)} = 1 :- node(Y), Y != 0.
\end{lstlisting}

For time windows it is important at which exact step the node is serviced, because at this step, the vehicle must be within a node's window. Therefore the rule randomly chooses such a step among all the visits of the servicing vehicle.\\

\begin{lstlisting}
% Choose which cycle to use next
useVehicle(0,0,C,V) :- assign(C,V), not notUseVehicle(0,C,V).
useVehicle(N,T,C,V) :- assign(C,V), not notUseVehicle(N,C,V), useVehicle(N-1,_,C',V), reachedAt(0,T,_,C'), vehicleNum(N), T > 0.
notUseVehicle(N,C,V) :- useVehicle(N,_,C',V), assign(C,V), C != C'.
notUseVehicle(N+1,C,V) :- useVehicle(N,_,C,V).
notUseVehicle(N'+1,C,V) :- useVehicle(N,_,C,V), useVehicle(N',_,C',V), N' > N.
\end{lstlisting}

The purpose of the rules in the listing is to create an ordering in which a vehicle processes its cycles. The first statement starts with a random cycle at time 0. Line 3 continues with the next (random) cycle, whenever the previous one has been calculated and carries over the current time.\\
The last three statements avoid that two cycles are selected at the same time, or a cycle is selected twice.\\

\begin{lstlisting}
% Start cycle
leftAt(0,T,0,C) :- useVehicle(_,T,C,_).
\end{lstlisting}

Now a timeline for each cycle is started, by stating that the depot has been left at time 0 (relative to the time the cycle starts).\\

\begin{lstlisting}
% Calculate times the nodes are reached. First for non-serviced nodes and then for serviced. For serviced nodes several time windows may be possible, choose just one
reachedAt(Y,T,N,C) :- routeOrder(X,Y,N,C), leftAt(X,O,N-1,C), duration(X,Y,D),num(N), T = O + D, not servicedAtTw(N,C), timeLimit(L), T < 2 * L. %2* L is just a bound, may produce wrong results in edge cases
reachedAt(Y,A,N,C) :- routeOrder(X,Y,N,C), leftAt(X,O,N-1,C), duration(X,Y,D),num(N), T = O + D, servicedAtTw(N,C), available(Y,A), A >= T, not alreadyReached(Y,A,N,C).
alreadyReached(X,T',N,C) :- reachedAt(X,T,N,C), available(X,T'), T != T'.
\end{lstlisting}

The next step is to calculate at which time every node in the cycle is reached. For each transition from node to node, the edge duration is added. If the node is not serviced at this step, this is the arrival time (line 2), otherwise a random time window at the arrival time or later is chosen as others can't be met (line 3).\\
The last statement ensures that only one time window per node is chosen.\\
Note that if no time window can be hit, the calculation stops at this point.\\

\begin{lstlisting}
% Now calculate when the nodes are left, if serviced simply add the service time.
leftAt(X,T,N,C) :- reachedAt(X,T,N,C), num(N), not servicedAtTw(N,C).
leftAt(X,T,N,C) :- reachedAt(X,O,N,C), num(N), servicedAtTw(N,C),serviceTime(X,S), T = O + S.
\end{lstlisting}

Since before only the arrivals at a node have been calculated, the departures are still needed. Whenever a node is not serviced, the vehicle is assumed to leave immediately (first line), otherwise it will stay for the duration of the service time (second line).\\

\begin{lstlisting}
% Now exclude all models where not every node is reached
:- routeOrder(_,X,N,C), not reachedAt(X,_,N,C).
\end{lstlisting}

Whenever there is a node, where no time window could be met, we stop.\\

\begin{lstlisting}
%Find out when the first node is hit. This is necessary since we don't have to leave the depot at time 0.
% When we have to leave is determined by the first serviced node. (Note that )
travelTime(X,T,N,C) :- servicedAtTw(N,C), reachedAt(X,T,N,C), useVehicle(0,_,C,V).
% Now calculate back
travelTime(X,O,N-1,C) :- travelTime(Y,T,N,C), not servicedAtTw(N-1,C), routeOrder(X,Y,N,C), duration(X,Y,D), O = T-D.
\end{lstlisting}
For the arrival times, it is assumed that a vehicle leaves the depot at time 0. For the first cycle, this may not be necessary, since the actual departure time depends on the first node serviced, and the vehicle may leave at a later point. This has an impact on the total travel duration.\\
This case is managed by the lines above. The rule in line 3 fixes the arrival time for all serviced nodes. As we have to meet the time windows, there is no flexibility for these nodes.\\
The next statement now traces the steps backwards to the latest possible departure time from non-serviced nodes, eventually arriving at the depot.\\

\begin{lstlisting}
% Now verify that it does not take too long
:- travelTime(0,T,0,C), assign(C,V), assign(C',V), reachedAt(0,T',_,C'), timeLimit(L), T' - T > L.
\end{lstlisting}
The previous calculations deliver the necessary information for this rule to check the time constraint.\\

\begin{lstlisting}
% Cycles where we don't service anyone are useless
:- leave(C), not servicedAtTw(_,C).
\end{lstlisting}
It may be possible, that there exist cycles, where no node is serviced, and which therefore do not contribute. In this case we can stop, as this won't become an optimal solution.

There may be many ways to order the cycles and many valid timelines, depending on the size of the time windows. This makes instances of the time window extension considerably harder to solve compared to normal VRPs.

\subsection{Visualization}
In order to make the results more accessible, a little visualisation script has been added, using the clingo python interface \cite{potasscopython}. The script ``visualizer.lp'' visualizes the solution in two forms:
\begin{itemize}
	\item Using the networkx \cite{networkx} library it produces a graphic representation of the solution. For this, each vehicle is assigned a color. The graph is drawn, where each node is in the color of the servicing vehicle and each edge's color is determined by the vehicles using it.
	\item One line with the node sequence for each vehicle is printed to the console.
\end{itemize}


\subsection{Instance Generator}
In order to test the scripts with larger instances, a Java program which creates instances of increasing sizes has been added. Different types of instances are created, with sizes $s \in [1..20]$, where the meaning of size depends on the instance type.\\
The source code can be found in ``generator/src/'', the generated instances can be found in ``generator/generated/''.

The following instance types are created by the program:
\begin{description}

\item[Connected Nodes]
This instance type produces a fully connected graph where $s + 2$ nodes are used. Each instance uses five vehicles, with costs, service times, and durations of 1.

\item[Connected Nodes Random]
Similar to ''Connected Nodes'' but uses random edge costs ranging from 1 to 50.

\item[Connected Vehicles]
This instance type produces a fully connected graph where $s$ vehicles are used. Each instance has 10 nodes, with costs, service times, and durations of 1.

\item[Connectedness Ratio]
This instance type produces a graph with a specific ''connectedness ratio'', i.e. how many of the possible edges are actually created ($s * 5$ percent). The edges are chosen randomly. Each instance uses 5 vehicles and 20 nodes. Durations, costs, and service times are 1.

\item[Connectedness Nodes]
Similar to the previous type, but uses $s + 2$ nodes and creates 30\% of possible edges.

\item[Cycle Number]
Produces $s$ cycles of length 10. $s$ vehicles are used and costs, service times, and durations are 1.

\item[Cycle Number Vehicle]
Similar to the previous type, but uses a fixed vehicle number of 5.

\item[Cycle Size]
This type uses cycles as well, but here size determines the number of nodes per cycle. Five such cycles are created as well as five vehicles. Costs, service times, and durations are 1.

\item[Simple Cycle]
This type creates one cycle with $s * 5$ nodes using one vehicle. Costs, service times, and durations are 1.

\end{description}


\section{Test Instance Results}
In order to evaluate how well the encoding works, the aforementioned test instances were run with increasing sizes using  Potassco's clingo 5.2.0 \cite{potassco}. Table \ref{table:benchmark} shows the maximum size that was solvable for the VRP and VRPTW encodings in a five minute timeframe. The tests were run on an Intel Core i5 with 3,3 GHz and 8 GB of RAM.\\
To give an intuition of the performance gain that was possible between the first and last version of the encoding, the table additionally contains results taken with the first version.\\
Please note, that not all instance types exist for the time window extension. Only deterministic types are used for VRPTW.

\begin{table}
\centering
\begin{tabular}{l | r | r | r}
Instance Type			& VRP v1	& VRP	& VRPTW\\
\hline
Connected Nodes			& 2	(16)	& 7	(145)	& 4 (88)\\
Connected Nodes Random	& 2 (16)	& 7	(145)	& -\\
Connected Vehicles		& 0			& 0 \footnote{While v1 stopped during grounding, the final encoding actually found the optimal solution, but did not manage to verify its optimality in time}	& 0\\
Connectedness Ratio		& 2 (71)	& 6	(163)	& -\\
Connectedness Nodes		& 16 (22)	& 18 (81) 	& -\\
Cycle Number			& 2	(9)		& 5	(130)	& 2 (299)\\
Cycle Number Vehicle	& 1	(1)		& 4	(51)	& -\\
Cycle Size				& 3 (32)	& 12 (293)	& 3\\
Simple Cycle			& 7	(40)	& 9	(113)	& 3 (300)\\
\end{tabular}
\caption{Solvable test instances. The first number is the maximum solvable instance size and the number in parenthesis is the runtime in seconds}
\label{table:benchmark}
\end{table}

The results show that considerable performance gain was possible due to the optimization efforts. Furthermore, the performance decreases drastically when solving VRPTW instances.

Unfortunately, the instances available on the internet\footnote{A good collection can be found at \url{http://neo.lcc.uma.es/vrp/vrp-instances/}} would require a greater transformation. The transformation would be time-consuming and the results would not be directly comparable with the original instance. Therefore only a rough comparison can be drawn.\\
The ''Simple Cycle'' instance is probably the easiest graph structure in the context of the VRP. The used encoding was able to solve an instance of size 9, which equals 45 nodes. This is rather small compared to current benchmark results, where instances using over 100 nodes can be optimally solved in under a minute \cite{benchmark}.


\section{Conclusion and Future Works}
Personally I learned a lot about ASP, leaving me with a deeper understanding of how solving works and how encodings can be optimized.\\
Regarding the results I have to conclude that the ASP approach chosen for this project requires more work for adequate performance. Many constraints in the VRP and especially the VRPTW require ordering and counting, which seem contrived and inefficient in the current version.\\
However, it does not provide conclusive evidence, that ASP is not applicable to this problem in general, as other approaches may yield better results.

During the project several issues were identified, where no solution could be found, but which may greatly impede performance. Additionally some ideas on how to increase performance were out of scope.\\
There are three things that are known to increase the solving time of this encoding:
\begin{enumerate}
	\item Loose limits; this may be improved by making the limits part of the problem instance and therefore the users responsibility. These limits include time limit (already part of the instance), cycle length limit and the maximum number of cycles. Loose limits cause a blow up during grounding.
	\item Symmetries; as mentioned before symmetries can widen the search space immensely. Although efforts have been made to break these symmetries, not all could be eliminated. Symmetries that are known to exist are the assignment of cycles to vehicles, servicing assignments and route directions.
	\item Number of edges; the impact of edges are twofold, they cause a blow up during the grounding phase and widen the search space. 
\end{enumerate}

While it is natural for larger instances to contain more edges, the impact could be softened by introducing a preprocessing step. The extra step may eliminate unnecessary edges (e.g. in a complete graph) or identify clusters and thereby divide the problem into smaller sub-problems.

It is also worth mentioning that during this project the constraints have been loosened to be more applicable to real world scenarios. Especially allowing nodes to be visited more than once allows for a larger set of instances to be solvable, but has a huge impact on performance.\\
Tightening this constrain would greatly reduce the number of possible routes. Additionally one cycle would directly correspond to one vehicle, removing the assignment symmetry. Since a vehicle cannot have more than one cycle, we do not need an order for the edges we use, removing some symmetries, as well as the need for a step limit. Since we now know that for each node exactly one incoming and one outgoing edge is needed, route generation is easier as well.\\
A disadvantage would be, that some instances may require extra edges to be solvable, e.g. to allow leaving leaf nodes.

\bibliographystyle{unsrt}
\bibliography{documentation}

\end{document}